#! /usr/bin/python
# ~*~ coding=utf-8 ~*~

##
# Generates PDF file from a given SLA file - quick & dirty
#
# For more information,
# see https://impagina.org/scribus-scripter-api/pdf-export
# see https://wiki.scribus.net/canvas/Automatic_Scripter_Commands_list
#
# Usage:
# scribus -g -py pdf-gen.py --input input_file.sla --output output_file.pdf
#
# License: MIT
# (c) Martin Folkers
##

import scribus
import argparse

parser = argparse.ArgumentParser(
    description="Generates PDF file from a given SLA file - quick & dirty"
)

parser.add_argument(
    "--input",
    help="Takes SLA file under specified path"
)

parser.add_argument(
    "--output",
    help="Creates PDF file under specified path"
)

parser.add_argument(
    "--mode",
    default="screen",
    help="Output target, either 'screen' or 'print'"
)


if __name__ == "__main__":
    args = parser.parse_args()

    # Generate PDF
    # (1) Open SLA file
    scribus.openDoc(args.input)
    pdf = scribus.PDFfile()

    # (2) Determine filename ..
    # (a) .. from SLA file
    file_name = scribus.getDocName()[:-3] + 'pdf'

    # (b) .. if provided ..
    if args.output is not None:
        # .. from CLI argument
        file_name = args.output

    # (c) Assign filename
    pdf.file = file_name

    # (3) Configure PDF generation
    # (a) Generate thumbnails
    pdf.thumbnails = True

    # (b) Select 'screen' as target
    # 0 - screen (RGB)
    # 1 - printer (CMYK)
    pdf.outdst = 0 if args.mode == 'screen' else 1

    pdf.save()
    scribus.closeDoc()
