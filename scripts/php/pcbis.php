<?php

require_once('vendor/autoload.php');

use Fundevogel\Pcbis\Pcbis;
use Fundevogel\Pcbis\Helpers\A;
use Fundevogel\Pcbis\Helpers\Str;
use Fundevogel\Pcbis\Classes\Product\Product;
use Fundevogel\Pcbis\Utilities\Spreadsheets;

use Nicebooks\Isbn\IsbnTools;
use Shieldon\SimpleCache\Cache;

class KNVClient
{
    /**
     * Modus operandi
     *
     * @var string
     */
    private $mode;


    /**
     * Current issue
     *
     * @var string
     */
    private $issue;


    /**
     * Current category
     *
     * @var string
     */
    private $category;


    /**
     * Source path
     *
     * @var string
     */
    private $root;


    /**
     * Destination path
     *
     * @var string
     */
    private $dist;


    /**
     * JSON source file
     *
     * @var str
     */
    private $file;


    /**
     * Age ratings
     *
     * @var array
     */
    private $ageRatings = [];


    /**
     * Failed API calls
     *
     * @var array
     */
    private $failures = [
        'data' => [],
        'cover' => [],
    ];


    /**
     * Object granting access to KNV's API
     *
     * @var \Fundevogel\Pcbis\Pcbis
     */
    private $api = null;


    /**
     * Cache driver for storing API calls
     *
     * @var \Shieldon\SimpleCache\Cache
     */
    private $cache = null;


    /**
     * ISBN tools for validating & formatting ISBNs
     *
     * See https://github.com/nicebooks-com/isbn
     *
     * @var \Nicebooks\Isbn\IsbnTools
     */
    private $isbnTools = null;


    /**
     * Constructor
     *
     * @param string $mode Modus operandi
     * @param string $issue Current issue
     * @param string $category Current category
     * @throws Exception
     * @return void
     */
    public function __construct($mode, $issue, $category)
    {
        if ($mode === null || $issue === null || $category === null) {
            throw new Exception('Please enter valid parameters.');
        }

        # Determine mode
        $this->mode = $mode;

        # Determine issue
        $this->issue = $issue;

        # Determine category
        $this->category = $category;

        # Implement caching
        # (1) Determine parent directory ..
        $cacheDirectory = __DIR__ . '/.cache';

        # .. and create it (if necessary)
        if (!is_dir($cacheDirectory)) {
            mkdir($cacheDirectory);
        }

        # (2) Initalize cache driver
        $this->cache = new Cache('file', ['storage' => $cacheDirectory]);

        # Set paths
        # (1) Base path
        $this->base = realpath(dirname(__DIR__) . '/../issues/' . $issue);

        # (2) Source & destination path
        $this->root = $this->base . '/src';
        $this->dist = $this->base . '/dist';

        # Authenticate with KNV's API
        # (1) Load credentials
        $credentials = json_decode(file_get_contents(__DIR__ . '/../../login.json'), true);

        # (2) Initialize API
        $this->api = new Pcbis($credentials);

        # Initialize ISBN toolbox
        $this->isbnTools = new IsbnTools();
    }


    /**
     * Main function
     *
     * @return void
     */
    public function run(): void
    {
        if ($this->mode == 'fetching') {
            # Determine source file
            $file = sprintf($file = '%s/csv/%s.csv', $this->root, $this->category);

            if (!file_exists($file)) {
                throw new Exception(sprintf('Invalid file: "%s"', $file));
            }

            $data = [];

            # Retrieve data for all books
            foreach (Spreadsheets::csvOpen($file) as $item) {
                # Clean ISBN (primarily for VLB-TIX support)
                $isbn = preg_replace('/[^0-9]/', '', $item['ISBN'] ?? $item['ISBN 13']);

                echo sprintf('Processing "%s":', $isbn);
                echo "\n";

                echo 'Fetching data from API ..';

                try {
                    # Load bibliographic data
                    $book = $this->loadBook($isbn);

                    if (is_null($book)) {
                        throw new Exception(sprintf('API error, skipping "%s"', $isbn));
                    }

                    # Get sorting key
                    $sortKey = '';

                    if (array_key_exists('IndexAutor', $book->data)) {
                        if (is_string($book->data['IndexAutor'])) {
                            $sortKey = $book->data['IndexAutor'];
                        } else {
                            $sortKey = $book->data['IndexAutor'][0];
                        }
                    }

                    # Export dataset
                    $set = array_merge([
                        'ISBN' => $this->isbnTools->format($isbn),
                        'Sortierung' => trim($sortKey),
                    ], $book->export());

                    # Keep only first publisher
                    $set['Verlag'] = $book->publisher()->toString();

                    # Determine age recommendation ..
                    $age = '';

                    # .. (1) except for calendars
                    if (!$book->isCalendar()) {
                        $age = $book->age()->toString();

                        # Handle empty age ratings
                        if (empty($age)) {
                            $age = 'Keine Altersangabe';
                        }
                    }

                    # .. (2) and apply it
                    $set['Altersempfehlung'] = $age;

                    $data[] = $set;

                    echo ' done.';
                    echo "\n";

                    echo 'Downloading cover ..';

                    # Download book cover
                    if ($book->downloadCover(sprintf('%s/images/%s.jpg', $this->dist, Str::slug($book->title()))) === false) {
                        # Add ISBN when download fails
                        $this->failures['cover'][] = $isbn;
                    }

                    echo ' done.';
                    echo "\n";
                    echo "\n";

                } catch (Exception $e) {
                    # Add ISBN when fetching data fails
                    $this->failures['data'][] = $isbn;

                    echo ' failed!';
                    echo "\n";
                }
            }

            echo 'Process complete!';
            echo "\n";
            echo "\n";

            # Sort by author's last name
            $data = A::sort($data, 'Sortierung', 'asc');

            # Remove sorting key
            foreach ($data as &$item) {
                unset($item['Sortierung']);
            }

            # Save data for further processing
            # (1) Store dadasets
            $this->jsonStore($data, sprintf('%s/json/%s.json', $this->root, $this->category), true);

            # (2) Store failed ISBNs
            $failures = [];

            if (file_exists($failedFile = $this->base . '/meta/failures.json')) {
                $failures = json_decode(file_get_contents($failedFile), true);
            }

            $this->jsonStore(array_merge($failures, $this->failures), $failedFile);
        }

        if ($this->mode == 'processing') {
            # Determine source file
            $file = sprintf('%s/json/%s.json', $this->root, $this->category);

            if (!file_exists($file)) {
                throw new Exception(sprintf('Invalid file: "%s"', $file));
            }

            # Load age ratings
            if (file_exists($ageRatingsFile = $this->base . '/config/age-ratings.json')) {
                $this->ageRatings = json_decode(file_get_contents($ageRatingsFile), true);
            }

            # Load duplicates file
            if (file_exists($duplicatesFile = $this->base . '/config/duplicates.json')) {
                $duplicates = json_decode(file_get_contents($duplicatesFile), true);
            }

            # Prevent duplicate ISBNs per category
            $isbns =  [];

            # Load data from JSON file
            $raw = json_decode(file_get_contents($file), true);

            $data  = [];

            # Retrieve data for every book
            foreach ($raw as $item) {
                $isbn = $item['ISBN'];

                # Block duplicate ISBNs across category
                if (in_array($isbn, $isbns) === true) {
                    continue;
                }

                # Block duplicate ISBNs across categories
                if (isset($duplicates[$isbn]) && in_array($this->category, $duplicates[$isbn])) {
                    continue;
                }

                echo sprintf('Processing "%s" ..', $isbn);

                # Setup basic information
                # (1) International Standard Book Number
                # (2) Keep comma-separated author (for sorting)
                $node = ['ISBN' => $isbn];

                try {
                    # Fetch bibliographic data from API
                    $book = $this->api->load($isbn);

                    # Combine all information for ..
                    # (1) .. template files
                    $node['AutorInnen'] = $book->author()->toString();
                    $node['Kopfleiste'] = $this->buildHeading($book);
                    $node['Inhaltsbeschreibung'] = $this->buildDescription($book);
                    $node['Mitwirkende'] = $this->buildParticipants($book);
                    $node['Informationen'] = $this->buildInformation($book);
                    $node['Abschluss'] = $this->buildClosing($book);
                    $node['Preis'] = $book->retailPrice();
                    $node['@Cover'] = Str::slug($book->title()) . '.jpg';

                    # (2) .. general use
                    $node['Titel'] = $book->title();
                    $node['Untertitel'] = $book->subtitle();
                    $node['Verlag'] = $book->publisher()->toArray()[0];

                    # Store data record
                    $data[]  = $node;

                    # Mark ISBN as processed
                    $isbns[]  = $isbn;

                    echo ' done.';
                    echo "\n";

                } catch (\Exception $e) {
                    echo ' failed!';
                    echo "\n";
                }
            }

            # Create updated JSON file
            $jsonFile = sprintf('%s/json/%s.json', $this->dist, $this->category);

            $this->jsonStore($data, $jsonFile, true);
        }
    }


    /**
     * Loads bibliographic data, either from cache or API
     *
     * @param string $isbn
     * @return \Pcbis\Products\Product
     */
    private function loadBook(string $isbn)
    {
        $data = $this->cache->get($isbn);

        # If entry was not cached before ..
        if (is_null($data)) {
            # (1) .. fetch data from API
            $book = $this->api->load($isbn);

            # (2) .. store data in cache
            $this->cache->set($isbn, $book->data);

            return $book;
        }

        # .. otherwise, retrieve data from cache
        return $this->api->load($data);
    }


    /**
     * Builds content of article's 'header' part
     *
     * @param \Fundevogel\Pcbis\Classes\Product\Product $book
     * @return string
     */
    private function buildHeading(Product $book): string
    {
        # Determine title & subtitle
        # (1) Store title
        $heading = $book->title();

        # (2) Add subtitle (if present)
        $subtitle = $book->subtitle();

        if (!empty($subtitle)) {
            $heading .= '. ' . $subtitle;
        }

        return $heading;
    }


    /**
     * Builds content of article's 'main body' part
     *
     * @param \Fundevogel\Pcbis\Classes\Product\Product $book
     * @return string
     */
    private function buildDescription(Product $book): string
    {
        # Store all available descriptions as string
        return A::join($book->description()->toArray(), "\n");
    }


    /**
     * Builds content of article's 'participants' part
     *
     * @param \Fundevogel\Pcbis\Classes\Product\Product $book
     * @return string
     */
    private function buildParticipants($book): string
    {
        $participants = '';

        # TODO: Determine involved people
        // $people = [
        //     $book->illustrator(),
        //     $book->drawer(),
        //     $book->photographer(),
        //     $book->translator(),
        //     $book->editor(),
        //     $book->participant(),
        //     $book->original(),
        // ];

        // $participants = '';

        # TODO: Determine involved people
        // $people = [
        //     $book->narrator(),
        //     $book->composer(),
        //     $book->producer(),
        //     $book->director(),
        //     $book->participant(),
        // ];

        // $participants = '';

        // $participants = $book->author();

        return $participants;
    }


    /**
     * Builds content of article's 'aside' part
     *
     * @param \Fundevogel\Pcbis\Classes\Product\Product $book
     * @return string
     */
    private function buildInformation(Product $book): string
    {
        # Build info string
        $year = $book->releaseYear();
        $publisher = $book->publisher();

        # Determine preposition, depending on publisher
        $preposition = 'bei';

        if (Str::contains(Str::lower($publisher), 'verlag')) {
            $preposition = 'im';
        }

        # Build information depending on category
        $specifics = '.';

        if ($book->isBook()) {
            $specifics = sprintf('; %s und %s Seiten stark', $book->binding(), $book->pageCount());
        }

        if ($book->isMedia()) {
            $specifics = sprintf('; %s', $book->duration());
        }

        if ($book->isCalendar()) {
            # TODO: Add dimensions
            $specifics = sprintf('und %scm groß', $book->dimensions());
        }

        return sprintf('%s %s %s erschienen%s.', $year, $preposition, $publisher, $specifics);
    }


    /**
     * Builds content of article's 'footer' part
     *
     * @param \Fundevogel\Pcbis\Classes\Product\Product $book
     * @return string
     */
    private function buildClosing(Product $book): string
    {
        $isbn = $book->isbn();

        if ($book->isCalendar()) {
            return $isbn;
        }

        # Determine age recommendation
        $age = isset($this->ageRatings[$isbn])
            ? $this->ageRatings[$isbn]
            : $book->age()
        ;

        return $isbn . ' - ' . $age;
    }


    /**
     * Shared
     */

    /**
     * Saves data as JSON file
     *
     * @param array $data Data to be stored
     * @param string $file Path to JSON file
     * @param bool $valuesOnly Whether only values should be stored
     * @return void
     */
    private function jsonStore(array $data, string $file, bool $valuesOnly = false): void
    {
        if ($valuesOnly === true) {
            $data = array_values($data);
        }

        # Determine parent directory ..
        $directory = dirname($file);

        # .. and create it (if necessary)
        if (!is_dir($directory)) {
            mkdir($directory);
        }

        # Store data as JSON file
        # (1) Create file handle
        $file = fopen($file, 'w');

        # (2) Write JSON data
        fwrite($file, json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));

        # (3) Close file handle
        fclose($file);
    }
}


$mode = null;

if (isset($argv[1])) {
    $mode = $argv[1];
}

$issue = null;

if (isset($argv[2])) {
    $issue = $argv[2];
}

$category = null;

if (isset($argv[3])) {
    $category = $argv[3];
}

$object = (new KNVClient($mode, $issue, $category))->run();
