Aracari:
Mies van Hout - "Wär ich doch ..." auf Seite 16
Leo Timmers - "Die Elefanteninsel" auf Seite 23

Arctis Verlag:
Becky Albertalli; Adam Silvera - "Was ist mit uns" auf Seite 48
Kyra Groh - "Mein Leben als lexikalische Lücke" auf Seite 49
Adam Silvera - "Am Ende sterben wir sowieso" auf Seite 52

Atlantis Zürich:
Laura D'Arcangelo - "Herr Bert und Alfonso jagen einen Dieb" auf Seite 14

Atrium Verlag:
Norton Juster - "Die Abenteuer von Milo, Tack und Kackerlack" auf Seite 33
Onjali Q. Raúf - "Die Nachtbushelden" auf Seite 34

Carl-Auer:
Cosetta Zanotti - "Was Worte alles machen" auf Seite 24

Carlsen:
Axel Scheffler - "Pip und Posy: Die kleine Schnecke" auf Seite 8
Daniela Kunkel - "Das kleine WIR im Kindergarten" auf Seite 17
Chris Owen - "Pandazamba" auf Seite 19
Lauren Castillo - "Igel und Schnuff" auf Seite 26
Constanze von Kitzing - "Ich bin anders als du" auf Seite 27
Marc-Uwe Kling - "Der Tag, an dem Papa ein heikles Gespräch führen wollte" auf Seite 27
Margit Auer - "Die Schule der magischen Tiere ermittelt 3: Der Kokosnuss-Klau (Zum Lesenlernen)" auf Seite 30
Margit Auer - "Die Schule der magischen Tiere - Endlich Ferien 6: Hatice und Mette-Maja" auf Seite 33
Martin Muser - "Nuschki" auf Seite 34
Jeanne Birdsall - "Die Penderwicks im Glück  (Die Penderwicks 5)" auf Seite 37
Cornelia Franz - "Calypsos Irrfahrt" auf Seite 38
Katherine Rundell - "Mitten im Dschungel" auf Seite 41
Kiera Stewart - "Dumme Ideen für einen guten Sommer" auf Seite 41
Daniel Höra - "Carlsen Clips: Von wegen Freundschaft!" auf Seite 45
Roberta Marasco - "Alles ganz normal" auf Seite 45
Rick Riordan - "Die Abenteuer des Apollo, Der Turm des Nero" auf Seite 46
Jenn Bennett - "Wiedersehen mit Lucky" auf Seite 48
Eleanor Levenson - "Politik" auf Seite 60
Günther Jakobs - "Die Eiersucherei" auf Seite 75
Anton Poitier - "Kreatives Bastelset: Ostern" auf Seite 76
Andrea Pöter - "Mein bunter Osterblock" auf Seite 77
Christian Tielmann - "Wartemal wird Osterschwein" auf Seite 78
Daniel Sohr; Katharina E. Volk; Günther Jakobs; Rüdiger Paulsen; Heribert Schulmeyer; Jette Kleeberg; Ana Zabo; Miriam Cordes; Julian Meyer - "Die schönsten Hasengeschichten" auf Seite 78

Der Audio Verlag, DAV:
Mikki Daughtry; Rachael Lippincott - "All This Time - Lieben heißt unendlich sein, 1 Audio-CD, 1 MP3" auf Seite 68
Jenny Pearson - "Die unglaubliche Wunderreise des Freddie Yates, 1 Audio-CD, 1 MP3" auf Seite 70
Maria Isabel Sanchez Vegara - "Little People, Big Dreams® - Teil 2: Ella Fitzgerald, Jane Austen, Coco Chanel, Muhammad Ali, 1 Audio-CD" auf Seite 71

Diogenes:
Andrej Kurkow; Tania Goryushina - "Warum den Igel keiner streichelt" auf Seite 17
Julian Meyer; Timon Meyer - "Pauls Garten" auf Seite 18
Reiner Zimnik - "Der Bär auf dem Motorrad" auf Seite 24
Bernhard Schlink - "20. Juli" auf Seite 52
Benedict Wells - "Hard Land" auf Seite 53

Dressler Verlag GmbH:
Magdalena Hai - "Die Nimmersattkatze" auf Seite 26

DTV:
Jan Strathmann - "Milla und der Nashornbus" auf Seite 28
Holly Goldberg Sloan; Meg Wolitzer - "An Nachteule von Sternhai" auf Seite 38
Malin Klingenberg - "Elchtage" auf Seite 39
Frida Nilsson - "Sasja und das Reich jenseits des Meeres" auf Seite 40
Jason Reynolds - "Asphalthelden" auf Seite 40
Kate O'Shaughnessy - "Das Glück wartet nur bis um vier" auf Seite 40
Juli Zeh - "Socke und Sophie - Pferdesprache leicht gemacht" auf Seite 42
Jenny Han - "Zitronensüß" auf Seite 44
Alison McGhee - "Wie man eine Raumkapsel verlässt" auf Seite 45
Jason Reynolds - "Ghost" auf Seite 46
Laura Creedle - "Die Liebesbriefe von Abelard und Lily" auf Seite 49
Lauren Oliver - "Broken Things - Alles nur (k)ein Spiel" auf Seite 50
Frank Schwieger - "Ich, Kleopatra, und die alten Ägypter" auf Seite 61

Ellermann:
Sinem Sasmaz; Leonard Erlbruch - "Und wo sind meine Punkte?" auf Seite 21

EMF Edition Michael Fischer:
Jenny Strömstedt - "Kinder verändern die Welt" auf Seite 66
Simone Leiss-Bohn - "Mission Ostern - Finde den Eierdieb!" auf Seite 76
Thade Precht - "111 x Papierfalten - Ostern" auf Seite 77

Gerstenberg Verlag:
Liz Garton Scanlon; Audrey Vernick - "Fünf Minuten" auf Seite 14
Silke Lambeck - "Herr Röslein kommt zurück" auf Seite 34
Judith Burger - "Ringo, ich und ein komplett ahnungsloser Sommer" auf Seite 37
Kathleen Vereecken - "Alles wird gut, immer" auf Seite 42
Dirk Reinhardt - "Perfect Storm" auf Seite 51
Virginie Aladjidi - "Mammut, Urmensch, Höhlenbär" auf Seite 57
Jan Paul Schutten - "Das Weltall" auf Seite 60
Kristin Roskifte - "Alle zählen" auf Seite 65
Romana Romanyschyn - "Sehen" auf Seite 65

headroom sound production:
Sandra Pfitzner - "Abenteuer & Wissen: Sophie Scholl, Audio-CD" auf Seite 70
Sandra Pfitzner - "Abenteuer & Wissen: Maria Sibylla Merian, Audio-CD" auf Seite 71

Hummelburg Verlag:
Oskar Kroon - "Warten auf Wind" auf Seite 39
Andrea Schomburg - "Die Spur zum 9. Tag" auf Seite 41

Igel-Records:
Thomas Taylor - "Malamander. Die Geheimnisse von Eerie-on-Sea, 5 Audio-CD" auf Seite 72

Jacoby & Stuart:
Fleur Daugey; Olivier Charpentier - "Freiheit!" auf Seite 38
Eva Rottmann - "Mats & Milad" auf Seite 51

Jumbo Neue Medien:
Patricia Martín - "Was versteckt sich in deinem Bauch, Mama?" auf Seite 7
Valérie Weishar-Giuliani - "Mein Bär Noah und ich" auf Seite 9
Ilan Brenman - "Komm, wir wollen spielen!" auf Seite 12
Maria Gíron - "Arthur und der Elefant ohne Erinnerung" auf Seite 15
Susanna Isern - "Das große Buch der allergrößten Schätze" auf Seite 16
Florian Pigé - "So klein" auf Seite 20
Carla Swiderski; Hanna Müller; Hannah Stollmayer - "Ricki in der Tintenwolke" auf Seite 23
Hubert Schirneck - "Zwillinge im Doppelpass. Ulf und Kathi im Fußballfieber" auf Seite 35
Markus Osterwalder; Dorothée Böhlke - "Bobo Siebenschläfer. Zusammen sind wir stark, Audio-CD" auf Seite 70

Jungbrunnen-Verlag:
Helga Bansch - "Der PUPU" auf Seite 11

Kibitz, Berlin:
Volker Schmitt - "Zack!" auf Seite 55

Kindermann:
Julia Nüsch - "Der fleißige Mistkäfer und die Träume der Anderen" auf Seite 19

Klett Kinderbuch Verlag:
Pija Lindenbaum - "Wir müssen zur Arbeit" auf Seite 18
Sabine Lemire - "Mira #familie #paris #abschied" auf Seite 55
Katharina von der Gathen - "AnyBody" auf Seite 66

Kosmos (Franckh-Kosmos):
Lisa Apfelbacher; Regina Schwarz - "Guck mal. Frühling, Sommer, Herbst und Winter" auf Seite 58

Loewe Verlag:
Suzanne Lang - "Jim hat keinen Bock" auf Seite 17
Hans-Christian Schmidt - "Wer von euch?" auf Seite 21
Amelie Benn - "Bildermaus - Geschichten vom ersten Schultag" auf Seite 30
Ursula Poznanski - "Layers" auf Seite 51
London Shah - "Water Rising (Band 1) - Flucht in die Tiefe" auf Seite 52
Claire Freedman - "Der glücklichste Hase der Welt" auf Seite 75

Magellan:
Chris Chatterton - "Hallo Boss!" auf Seite 13
Rachel Bright - "Der Sorgosaurus" auf Seite 13
Megan Cooley Peterson - "Lügentochter" auf Seite 50
Andreas Thamm - "Wenn man so will, waren es die Aliens" auf Seite 53
Anna Elisabeth Albrecht; Susanne Rebscher - "Abenteuer Welterbe - Entdecke besondere Orte in Deutschland" auf Seite 57

mairisch Verlag:
Finn-Ole Heinrich; Dita Zipfel - "Schlafen wie die Rüben" auf Seite 63

mareverlag:
Olli Jalonen - "Die Himmelskugel" auf Seite 49
Till Hein - "Crazy Horse" auf Seite 59

Midas:
Camille Andros - "Das Kleid und das Mädchen" auf Seite 11
Andrea Beaty - "Rosie Rosin, Erfinderin" auf Seite 12
Katie Cottle - "Der blaue Riese" auf Seite 13
Lara Albanese - "Atlas des Weltalls" auf Seite 57
Khalil Gibran - "Der Prophet" auf Seite 63
José Jorge Letria - "Der Krieg" auf Seite 64

NordSüd Verlag:
Hans de Beer - "Kleiner Eisbär. Lars und die Pandabären" auf Seite 12
Rebecca Gugger - "Der Berg" auf Seite 15
Valeri Gorbachev - "Die gute Pute" auf Seite 15
Sean Julian - "Normans erster Tag im Dinokindergarten" auf Seite 16
Johanna Ries - "Die Fleckenfeder" auf Seite 20
Carla Haslbauer - "Die Tode meiner Mutter" auf Seite 63
Oliver Jeffers - "Was wir bauen" auf Seite 64
Baptiste & Miranda Paul - "Frieden" auf Seite 65

Oetinger:
Astrid Lindgren - "Ferien auf Saltkrokan. Als Tjorven einen Seehund bekam" auf Seite 18
Bianca Schulze - "Nicht den Drachen wecken" auf Seite 22
Steve Small - "Mit dir ist sogar Regen schön" auf Seite 23
Jujja Wieslander - "Mama Muh und Krähe werden Freunde" auf Seite 24
Paul Maar - "Der kleine Troll Tojok" auf Seite 28
Stefanie Taschinski - "Familie Flickenteppich 3. Wir machen Ferien" auf Seite 35
Aimée Carter - "Die Erben der Animox 1. Die Beute des Fuchses" auf Seite 37
Kirsten Boie - "Dunkelnacht" auf Seite 48
Peter Wohlleben - "Kommst du mit nach draußen?" auf Seite 61
Sabine Praml - "Wenn sieben kleine Badehasen quietschfidel ans Wasser rasen" auf Seite 76
Susanne Weber - "Die kleine Eule. Auwei, was ist das für ein Ei?" auf Seite 78

Oetinger Media:
Astrid Lindgren - "Ferien auf Saltkrokan. Als Tjorven einen Seehund bekam, 1 Audio-CD" auf Seite 69
Sven Nordqvist - "Pettersson und Findus. Unsere schönsten Abenteuer, 5 Audio-CD" auf Seite 69
Peter Wohlleben - "Kommst du mit nach draußen?, 2 Audio-CD" auf Seite 73

Oetinger Taschenbuch:
Kristina Aamand - "Wenn Worte meine Waffe wären" auf Seite 44

Peter Hammer Verlag:
Hanna Jansen - "Hasen in der Nase" auf Seite 27
Andrea Karimé - "Sterne im Kopf und ein unglaublicher Plan" auf Seite 39
Patrice Nganang - "Spur der Krabbe" auf Seite 50
Henning Wagenbreth - "Rückwärtsland" auf Seite 55

Ravensburger Verlag:
Sandra Grimm - "Ich bin die kleine Biene" auf Seite 6
Frauke Nahrgang - "Sachen suchen, Sachen fühlen: Im Wald" auf Seite 7
Kathrin Lena Orso; Jens Ohrenblicker - "Wer geht wo aufs Klo?" auf Seite 8
Monika Neubacher-Fesser; Monika Neubacher- Fesser - "Mein allererstes Rasselbuch" auf Seite 8
Gerlinde Wiencirz - "Ich bin das kleine Kaninchen" auf Seite 9
Manfred Mai - "Leons erster Schultag" auf Seite 30
Moira Butterfield - "Die bunte Welt der Flaggen" auf Seite 58
Andrea Erne - "Wieso? Weshalb? Warum? Alles über Roboter (Band 47)" auf Seite 59
Peter Nieländer - "Wieso? Weshalb? Warum? Mein junior-Lexikon: Urlaub" auf Seite 60
Cornelia Frank - "Wer bemalt das Osterei?" auf Seite 75

Silberfisch:
Adam Baron - "Auftauchen, 6 Audio-CD" auf Seite 68
Sabine Bohlmann - "Ein Mädchen namens Willow, 3 Audio-CD" auf Seite 68
Martin Muser - "Nuschki, 1 Audio-CD" auf Seite 69
Otfried Preußler - "Der kleine Wassermann, 2 Audio-CD" auf Seite 71
Annika Scheffel - "Sommer auf Solupp, 3 Audio-CD" auf Seite 72
Oliver Scherz - "Wir sind nachher wieder da, wir müssen kurz nach Afrika - Autorenlesung, 1 Audio-CD" auf Seite 72

Südpol Verlag:
Andrée Poulin - "Zwei Jungs und eine Hochzeit" auf Seite 20

Tessloff:
Florian Huber; Uli Kunz - "Ozeane" auf Seite 59

Tulipan:
Elsa Klever - "Tier hier" auf Seite 7
Nini Alaska - "Zickenzeit" auf Seite 11
Will Gmehling - "Bahnhof am Ozean" auf Seite 14
Inka Pabst - "Elfi, das traurige Krokodil" auf Seite 19
Andrea Schomburg - "Ab hier kenn ich mich aus" auf Seite 22
Nina Petrick - "Doppelt gebucht" auf Seite 31
Veronika Wiggert - "Fußballsommer" auf Seite 31
Beate Dölling - "Ab in die Rakete" auf Seite 33
Nikola Huppertz - "Schön wie die Acht" auf Seite 44

Tyrolia:
Reinhard Ehgartner - "Das kleine Farben-Einmaleins" auf Seite 6
Leonie Schlager - "Der Wassermann hat Zeit" auf Seite 21
Elisabeth Steinkellner - "Esther und Salomon" auf Seite 53

Ullmann Medien:
Svenja Doering - "Sound- und Fühlbuch Tiere der Wildnis (mit 6 Sounds und Fühlelementen)" auf Seite 6

Usborne Verlag:
Katie Daynes - "Erklär mir den Mond" auf Seite 58
Sarah Hull - "Ist das Kunst?" auf Seite 64

Von Hacht Verlag:
Isabelle Simler - "Süße Träumer" auf Seite 22

Woow Books:
Lena Frölander-Ulf - "Nelson Tigertatze" auf Seite 26

