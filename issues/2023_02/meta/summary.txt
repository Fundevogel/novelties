Achse, Wien:
Cornelia Lindner - "Wuschelkopf und Pupspopo" auf Seite 8
Patricia Strübin - "Was glitzert denn da?" auf Seite 58

Aracari:
Mies van Hout - "Heute bin ich - Stickerbuch" auf Seite 60

Arche Verlag:
Alexandra Rak - "Arche Kalender Starke Mädchen 2024" auf Seite 79
Neele Bösche - "Arche Märchen Kalender 2024" auf Seite 79
Sophie Härtling; Kristina Kreuzer - "Arche Kinder Kalender 2024" auf Seite 79

Arctis Verlag:
Alexander Kielland Krag - "Nur ein wenig Angst" auf Seite 47
Ashley Schumacher - "Für immer unter einem Himmel" auf Seite 49

ars edition:
wer kann das sein? - Im Einsatz Hör mal rein - "Hör mal rein, wer kann das sein? - Im Einsatz" auf Seite 6
Emilia Dziubak - "Ein Jahr im Wald" auf Seite 7
Andrea Peter - "Sooo viele Schuhe!" auf Seite 9
Caryl Hart - "Schwanensee und andere märchenhafte Ballettgeschichten" auf Seite 15
Natalia O'Hara - "Es war einmal ein Märchen" auf Seite 16
Kai Lüftner - "Der Verwechsling" auf Seite 24
David Farr - "Das Buch der gestohlenen Träume  (Das Buch der gestohlenen Träume 1)" auf Seite 38
Ico Romero Reyes - "Giftig!" auf Seite 57
 - "Abenteuer-Spaziergänge Im Winter" auf Seite 60
Rosa Bailey - "Das kleine Rentier und das Rotkehlchen" auf Seite 71

Atlantis Zürich:
Vera Eggermann - "Wer steigt ein?" auf Seite 7
Lorenz Pauli - "Entschuldigung" auf Seite 17
Werner Rohner - "Hier bin ich doch!" auf Seite 17
Janusz Korczak - "König Matz der Erste" auf Seite 35
Mimi Hachikai - "Im Schneeland" auf Seite 73

Baobab Books:
Jie Wei - "Damals, im Sommer" auf Seite 66

Beltz:
Philip Waechter - "Weltreise mit Freunden" auf Seite 20
Ida Mlakar Crnic - "In unserer Nähe wohnt ein Mädchen" auf Seite 23
Eoin Colfer - "Tim und das Geheimnis von Knolle Murphy" auf Seite 33
Wieland Freund - "Törtel, die Schildkröte aus dem McGrün" auf Seite 33
Susin Nielsen - "Adresse unbekannt" auf Seite 40
Juliane Pickel - "Rattensommer" auf Seite 48
Anna Skowronska - "Feuer" auf Seite 58
Axel Scheffler - "Einfach nett. Das fantasievolle Mitmachbuch" auf Seite 62
Michael Elpers - "Wenn Kinder unter Kindern leiden" auf Seite 64
Verena Carl - "Queere Kinder" auf Seite 64
Nicola Schmidt - "Trennung ohne Drama" auf Seite 65
Emma Thompson - "Jims brillante Weihnachten" auf Seite 76

Butzon & Bercker:
Susanne Maria Emka - "Der Ölbaum zu Betlehem" auf Seite 72

Carl-Auer:
Giorgio Volpe - "Freundschaft zu dritt" auf Seite 19

Carlsen:
Mascha Greune - "50 Reime für Kleine" auf Seite 7
Constanze von Kitzing - "Jetzt ist unser kleines Baby da" auf Seite 8
Christian Zimmer - "Hör mal (Soundbuch): Mein Riesenwimmelbuch" auf Seite 11
Wolfram Hänel - "LESEMAUS zum Lesenlernen Sammelbände: Wilde Silben-Geschichten" auf Seite 27
Usch Luhn - "Ponyherz und Herr Franz: Ponyherz und Herr Franz: Die geklaute Wurst" auf Seite 28
Andreas Steinhöfel - "Rico und die Tuchlaterne" auf Seite 29
Peter Stamm - "Theo und Marlen im Dschungel" auf Seite 29
Margit Auer - "Die Schule der magischen Tiere 14: Ach du Schreck!" auf Seite 32
Maja Konrad - "Henry Kolonko und die Sache mit dem Finden" auf Seite 34
Maria Kling - "Freddy und Flo 2: Das Geheimnis der muffigen Mumie" auf Seite 34
Juli Zeh - "Der war's" auf Seite 35
Charly Art - "Moonlight Wolves: Das Geheimnis der Schattenwölfe" auf Seite 37
Claudia Scharf - "Das Geheimnis von Nox 1: Licht, Schatten - Flederratten!" auf Seite 41
Rick Riordan - "Nico und Will - Reise ins Dunkel" auf Seite 44
Elise Bryant - "Zwölf Tage und immer" auf Seite 46
Bella Lack - "Let's Save the World - Geschichten junger Klimaaktivist_innen aus aller Welt" auf Seite 47
Nina Dulleck - "Die Schule der magischen Tiere: SELBERzeichnen" auf Seite 60
Christine Mildner - "Frag doch mal ... die Maus: Mein Rätselblock mit der Maus - Weltraum" auf Seite 61
Julia Hofmann - "Die Weihnachtsgeschichte für uns Kleine" auf Seite 74
Annette Moser - "Unser Weihnachtswunderhaus" auf Seite 75
Stefanie Neeb - "Coming Home for Christmas" auf Seite 75
Hannah Reynolds - "Eight Nights of Flirting" auf Seite 76

Diogenes:
Julian Meyer - "Bär & Hippo räumen auf" auf Seite 9

Dressler Verlag GmbH:
Cornelia Funke - "Tintenwelt 4. Die Farbe der Rache" auf Seite 46

DTV:
Yuval Noah Harari - "Warum die Welt nicht fair ist" auf Seite 38

dtv Deutscher Taschenbuch Verlag:
Alex Rühle - "Zippel macht Zirkus" auf Seite 0

Ellermann:
Kirsten Boie - "Regenbogenbunte Geschichten" auf Seite 22

Gabriel in der Thienemann-Esslinger Verlag GmbH:
Martina Baumbach - "Holunderweg: Ein Jahr im Holunderweg" auf Seite 22
Willemijn de Weerd - "Wie es Weihnachten wurde" auf Seite 77

Gerstenberg Verlag:
Eric Carle - "Das kleine Glühwürmchen" auf Seite 6
Stepha Quitterer - "Pepe und der Oktopus auf der Flucht vor der Müllmafia" auf Seite 41
Alexandra Litwina - "Alle einsteigen!" auf Seite 57
Kathrin Wolf - "In einem alten Haus in Berlin" auf Seite 58
Eric Carle - "Die kleine Raupe Nimmersatt - Wer versteckt sich im Schnee?" auf Seite 71

Jumbo Neue Medien:
Andreas H. Schmachtl - "Snöfrid aus dem Wiesental. Weihnachtszauber im Nordland, Audio-CD" auf Seite 68
Marko Simsa - "Klassik für kleine Ohren. Fantastische Märchenwelten" auf Seite 68
Audio-CD Fröhliche Lieder für Spiel und Spaß - "Fröhliche Lieder für Spiel und Spaß, Audio-CD" auf Seite 69
Audio-CD Schöner bunter Lieder-Herbst - "Schöner bunter Lieder-Herbst, Audio-CD" auf Seite 69
Audio-CD Sternschnuppen im Advent. 24 Lieder und Geschichten für die schönste Zeit des Jahres - "Sternschnuppen im Advent. 24 Lieder und Geschichten für die schönste Zeit des Jahres, Audio-CD" auf Seite 69

Kindermann:
Anna Kindermann - "Siegfried der Drachentöter" auf Seite 23

Knesebeck:
Kathrin Rohmann - "Der Geräuschehändler" auf Seite 0
Freya Blackwood - "Der Junge und der Elefant" auf Seite 13
Eva Dax - "Nicht mehr da" auf Seite 14
Cornelia Boese - "Wie eine Erbse kurzerhand die richtige Prinzessin fand" auf Seite 22
Clara Schaksmeier - "Heute geh ich in die Schule" auf Seite 29
Suza Kolb - "Moona" auf Seite 39
Fiona Longmuir - "Auf der Suche nach Emily McCrae" auf Seite 40
Robin Stevens - "Spionieren ist (k)ein Kinderspiel" auf Seite 44
Stephen Davies - "Mythen, Mumien und mächtige Pharaonen im Alten Ägypten" auf Seite 55

Leykam:
Petra Piuk - "Josch, der Frosch(könig) - Ein Nicht-Märchen" auf Seite 24

Limbion Books:
Jean Luc Fromental - "Miss Kat" auf Seite 52

Loewe Verlag:
Antoine de Saint-Exupéry - "Der kleine Prinz" auf Seite 18
Eva Hierteis - "Bildermaus - Rettung in den Bergen" auf Seite 27
Mary Pope Osborne - "Das magische Baumhaus junior (Band 35) - Leonardos Traum vom Fliegen" auf Seite 28
Henriette Wich - "Bildermaus - Geschichten von der Feuerwehr" auf Seite 30
Ursula Poznanski - "Oracle" auf Seite 48
Mariah Marsden - "Anne auf Green Gables" auf Seite 52
Debbie Tung - "Everything is okay" auf Seite 53
Dagmar Geisler - "Oje, wir haben Streit!" auf Seite 56
Raman Prinja - "Wunder am Sternenhimmel" auf Seite 57
Loewe Kreativ; Elodie Bossrez - "Gemeinsam malen - Tiere" auf Seite 61
Loewe Lernen und Rätseln; Cordula Kamb - "100 Gute-Laune-Rätsel bis zum Schulanfang" auf Seite 61

Magellan:
Jess Rose - "Das Gespenst will bleiben" auf Seite 18
Katja Alves - "Mafalda mittendrin - Zwei Mäuse auf der Flucht" auf Seite 32
Annette Herzog - "Ein Halstuch voller Lügen" auf Seite 39
Teresa Hochmuth - "Schwestern durch die Zeit - Luftschiffe im Bauch" auf Seite 43
Tina Blase - "Insel der wandernden Flüche - Skys Gabe" auf Seite 43
Maren Hasenjäger - "Mein großer Seekarten-Atlas - Entdecke die Welt der Meere und Ozeane" auf Seite 56
Teresa Hochmuth - "Holla Honigkuchenfee - Weihnachten ohne Liefer-Elch" auf Seite 73

Midas:
Alexandra Stewart - "Everest (Graphic Novel)" auf Seite 53
Laura Fraile - "Atlas der bedrohten Tiere" auf Seite 55
Alice Harman - "Mona Lisa & die anderen (Kunst für Kinder)" auf Seite 56
Joséphine Seblon - "Kunst? Kann ich! (Kunst für Kinder)" auf Seite 62

Minedition:
Kleiner Bär! Mach mit - "Mach mit, Kleiner Bär!" auf Seite 10
Regen Regen - "Regen, Regen, Sonnenschein" auf Seite 18
Karin Gruß - "Ein roter Schuh" auf Seite 38

Moon Notes:
Sherin Nagib - "Talking to the Moon" auf Seite 48

Moritz:
Thé Tjong-Khing - "Torte für alle!" auf Seite 19

NordSüd Verlag:
Jo Ellen Bogart - "Anton und der Gargoyle" auf Seite 13
Rashin; Thomas Bodmer - "Rumi. Dichter der Liebe" auf Seite 17
April Suddendorf - "Hippe Hexen und ihre zauberhaften Tiere" auf Seite 19
Torben Kuhlmann - "Die graue Stadt" auf Seite 35
Antonie Schneider - "Wann ist endlich Weihnachten?" auf Seite 76

Oetinger:
Cornelia Boese - "Ab geht die Post" auf Seite 6
Sven Nordqvist - "Der Weg nach Hause" auf Seite 16
Astrid Lindgren - "Pippi geht auf den Jahrmarkt" auf Seite 27
Kirsten Boie - "Thabo und Emma. 3 Abenteuer in einem Band" auf Seite 33
Sven Nordqvist - "Pettersson und Findus. Lustige Weihnachtsrätsel" auf Seite 62
Erhard Dietl - "Warum Weihnachtswunder manchmal ganz klein sind" auf Seite 72
Astrid Lindgren - "Die schönsten Weihnachtsgeschichten" auf Seite 74
Astrid Lindgren - "Ferien auf Saltkrokan. Pelle feiert Weihnachten" auf Seite 74
Astrid Lindgren - "Wichtel, Wunder, Winterzauber" auf Seite 75

Peter Hammer Verlag:
Susanne Straßer - "Waschbär wäscht Wäsche" auf Seite 10
Karin Koch - "Linas Geheimnis" auf Seite 39
Sascha Mamczak - "Überall Leben" auf Seite 44
Arne Rautenberg - "Mut ist was Gutes" auf Seite 65

Planet! in der Thienemann-Esslinger Verlag GmbH:
Sabine Bohlmann - "Ein Mädchen namens Willow 4: Nebeltanz" auf Seite 37
Lea Melcher - "Rubina Blackfield 1: Mit der Lizenz zum Spionieren" auf Seite 40

Ravensburger Verlag:
Sandra Grimm - "ministeps: Lesen, kuscheln, Augen zu" auf Seite 8
Patricia Mennen - "Wieso? Weshalb? Warum? Mein großes junior-Lexikon" auf Seite 9
Dan Clay - "Becoming a Queen (humorvolle LGBTQ+-Romance, die mitten ins Herz geht und dort bleibt)" auf Seite 46

Reprodukt:
Mawil - "Fritzi Frühaufsteher" auf Seite 16

Schaltzeit:
André Bouchard - "Ein Tag im Leben einer Fee" auf Seite 13

Schneiderbuch:
Christin-Marie Below - "Funkel, funkel, Weihnachtszeit. 24 Geschichten, Lieder, Gedichte und vieles mehr für den Advent" auf Seite 71

Silberfisch:
Otfried Preußler - "Meine große Klassiker-Hörspielbox, Audio-CD" auf Seite 68

Splitter:
Carbone; Hanna Reininger; Julien Monier - "Sam und die Geister. Band 1" auf Seite 51
Christophe Arleston - "Elfies Zauberbuch. Band 1" auf Seite 51
Maud Begon - "Der geheime Garten" auf Seite 51
Anne Montel - "Amelie und Mirko. Band 1" auf Seite 52

Tessloff:
Andrea Weller-Essers - "WAS IST WAS Meine Welt Band 11, Wir helfen und vertragen uns!" auf Seite 10
Sonja Meierjürgen - "WAS IST WAS Erstes Lesen easy! Band 14. Entdecke unsere Erde" auf Seite 28

Thienemann in der Thienemann-Esslinger Verlag GmbH:
Daniel Fehr - "So kam das mit dem Drachen" auf Seite 14
Daniel Napp - "Schnüffelnasen 1: Schnüffelnasen an Bord" auf Seite 24
Oliver Scherz - "Sieben Tage Mo" auf Seite 41
Davide Morosinotto - "Time Shifters" auf Seite 47
Tilman Spreckelsen - "Otfried Preußler" auf Seite 65

Tulipan:
Amrei Fiedler - "Fundhund" auf Seite 14
Veronika Wiggert - "Fußballfieber" auf Seite 30
Frauke Angel - "Tagebuch eines Überfliegers" auf Seite 32
Maike Harel - "Die Schule des abgrundtief Bösen" auf Seite 34
Nikola Huppertz - "Fürs Leben zu lang" auf Seite 43

Tyrolia:
Heinz Janisch - "Bleib noch eine Weile" auf Seite 23

ultramar Media:
Linda Dielemans - "Zeit der Bronze" auf Seite 55

Von Hacht Verlag GmbH:
Oliver Jeffers - "Da ist ein Gespenst im Haus" auf Seite 15
Linde Faas - "Irgendwo im Schnee" auf Seite 73

Woow Books:
Mikaël Brun-Arnaud - "Erinnerungen des Waldes" auf Seite 37
Katja Ludwig - "Innerlich bin ich aus Lakritze" auf Seite 64
Charles Dickens - "Eine Weihnachtsgeschichte" auf Seite 72

Zuckersüss:
Jackie Azúa Kramer - "Matteo glaubt an Einhörner" auf Seite 15

