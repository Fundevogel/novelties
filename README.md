# Recommended novelties

## About this project

Every year we put together two [lists of recommended novelties](https://fundevogel.de/en/recommendations) (~ 300 books) that become our spring and autumn editions.

This repository houses the workflow for doing so, all the logic for using [JSON files](https://en.wikipedia.org/wiki/JSON), exported from either `pcbis.de` or [VLB-Tix](https://vlbtix.de), fetching bibliographic data from their KNV's WSDL API (powered by [`php-pcbis`](https://github.com/fundevogel/php-pcbis)) and creating each issue from an individual [Scribus](https://www.scribus.net) template using the excellent [`ScribusGenerator`](https://github.com/berteh/ScribusGenerator) library.


## Setup

Clone this repository, including its submodules, like so:

```bash
# Clone repository
git clone --recursive https://codeberg.org/Fundevogel/novelties.git
```

Then, create a virtual environment and activate it:

```bash
# Change into project directory
cd novelties

# Create virtual environment
virtualenv -p python3 venv
source venv/bin/activate

# Install dependencies
python -m pip install -r requirements.txt
```


## Roadmap

- [x] Auto PDF generation from python
- [x] Adding masterpages via import script
- [x] Generate list: book>page, sorted by publisher
- [x] ~~Implement python WSDL workflow (eg using Zeep)~~
- [x] ~~Improve string-replacement~~
- [ ] Fix bug that causes imported pages to be "stacked" somehow (maybe Scribus)


:copyright: Fundevogel Kinder- und Jugendbuchhandlung
